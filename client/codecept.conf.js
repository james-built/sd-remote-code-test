const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// HEADLESS=true npx codecept run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './e2e/src/*.test.js',
  output: './e2e/output',
  helpers: {
    Puppeteer: {
      url: `http://${process.env.WEBSERVICEHOST || 'localhost'}:4200`,
      show: true,
      chrome: {
        args: [`--no-sandbox`],
      },
      windowSize: '1200x900'
    }
  },
  include: {
    I: './e2e/steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'e2e',
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}