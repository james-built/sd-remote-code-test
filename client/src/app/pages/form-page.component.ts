import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { switchMap, map, catchError, mergeMap } from 'rxjs/operators';
import { Observable, zip, of, throwError } from 'rxjs';
import { FormItemBase } from '../services/form-item-base';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityService } from '../services/entity.service';
import { FormTypeService } from '../services/form-type.service';
// <ngx-loading [show]="loading"></ngx-loading>
@Component({
  selector: 'app-form-page',
  template: `
    <ngx-loading [show]="loading"></ngx-loading>
    <div>
      <h2>Create {{ formName$ | async }}</h2>
      <app-dynamic-form (formSaved)="formSaved($event)" [formItems]="formItems$ | async"></app-dynamic-form>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormPageComponent implements OnInit {
  formName$: Observable<string>;
  loading = true;
  formItems$: Observable<FormItemBase<any>[]>;
  constructor(
    public route: ActivatedRoute,
    public entityService: EntityService,
    public formTypeService: FormTypeService,
    public router: Router,
    public cd: ChangeDetectorRef,
  ) {
  }

  ngOnInit() {

    // get form name from nav params
    this.formName$ = this.route.paramMap.pipe(
      map(param => param.get('formName'))
    );

    // load form
    this.formItems$ = this.formName$.pipe(
      switchMap(formName => {
        return this.formTypeService.getFormType(formName);
      }),
      switchMap(formType => {
        const formItems: Observable<FormItemBase<any>>[] = [];
        for (const formField of formType.formFields) {
          formItems.push(this.toFormItem(formField));
        }
        return zip(...formItems);
      }),
      map(formItems => {
        this.loading = false;
        this.cd.detectChanges();
        return formItems;
      }),
      catchError(err => {
        this.loading = false;
        alert(`${err}`);
        this.router.navigate(['']);
        return throwError(err);
      })
    );
  }

  toFormItem(formField): Observable<FormItemBase<any>> {
    const formItem = new FormItemBase(formField);
    // If the form item controlType is of type dropdown, we need to load the entities for the user to select.
    if (formItem.controlType == 'dropdown' && formItem.entityTypeId) {
      return this.entityService.getEntities(formItem.entityTypeId)
        .pipe(map(entities => {
          formItem.entities = entities;
          return formItem;
        }));
    } else {
      return of(formItem);
    }
  }

  formSaved(formValues) {
    alert(`Form saved!!! \n ${formValues}`);
    this.router.navigate(['']);
  }

}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/